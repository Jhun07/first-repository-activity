/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise3;

/**
 *
 * @author 2ndyrGroupA
 */
public class main {
     public static void main(String[] args) {
        System.out.println("-------Meeting Date-----------");
        MyDate date = new MyDate(2021, 03, 26);
        System.out.println("Meeting date: "+date);            
        System.out.println("The next day after meeting date is: "+date.nextDay());  
        System.out.println("And the next next day is: "+date.nextDay());
        System.out.println("And the next day and month after is: "+date.nextMonth());
        System.out.println("And the next day, month, and year after is: "+date.nextYear());
        System.out.println("And the previous day, month, and year before is: "+date.previousYear());
        System.out.println("");
        System.out.println("-------Birthday Date------------");
        MyDate birthdayDate = new MyDate(2009, 07, 03);
        System.out.println("My birthday is: "+birthdayDate);              
        System.out.println("The previous day of my birthday is: "+birthdayDate.previousDay());
        System.out.println("The previous day and month before my birthday is: "+birthdayDate.previousMonth());
        System.out.println("The previous day, month, and year before my birthday is: "+birthdayDate.previousYear());
        System.out.println("The next day, month, and year after my birthday is: "+birthdayDate.nextYear());
        System.out.println("");
        System.out.println("---------Holiday-----------");
        
        MyDate holiday = new MyDate(2014, 05, 01);
        System.out.println("Holiday date: "+holiday);       
        System.out.println("The previous day of this holiday is: "+holiday.previousDay());
        System.out.println("The previous day and month of this holiday is: "+holiday.previousMonth());
        System.out.println("The next day, month, and year of this holiday is: "+holiday.nextYear());
        System.out.println("");
        System.out.println("---------Day Off-----------");
        MyDate dayoff = new MyDate(2014, 02, 10);
        System.out.println("Day Off date: "+dayoff);       
        System.out.println("Yesterday date is: "+dayoff.previousDay());
        
        
 
     }
}
