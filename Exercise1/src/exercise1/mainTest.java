/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

/**
 *
 * @author 2ndyrGroupA
 */
public class mainTest {

    public static void main(String[] args) {

        System.out.println("-------------------------------------------------");
        Shape newShape = new Shape();
        System.out.println(newShape.toString());
        newShape.setColor("blue");
        System.out.println("The color is changed to:" + newShape.getColor());
        System.out.println(newShape.isFilled());
        System.out.println("-------------------------------------------------");
        System.out.println("");
        System.out.println("--------------------------------------------------------------------------------------------------------");
        Circle circle = new Circle(2.0);
        System.out.println(circle.toString());
        System.out.println("Radius of the circle is: " + circle.getRadius());
        System.out.println("Area of the circle is: " + circle.getArea());
        System.out.println("Color of the circle is: " + circle.getColor());
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------");
        Rectangle rectangle = new Rectangle(1.0f, 1.0f);
        System.out.println(rectangle.toString());
        System.out.println("Rectangle Length is: " + rectangle.getLength());
        System.out.println("Rectangle Width is: " + rectangle.getWidth());
        System.out.printf("The Area of the Rectangle is: %.2f%n", rectangle.getArea());
        System.out.printf("The Perimeter of the Rectangle is: %.2f%n", rectangle.getPerimeter());
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
       
        Square square = new Square(5.7f);
        System.out.println(square.toString());
        System.out.println("Color of the square is: " + square.getColor());
        System.out.println("Area of square = " + square.getArea());
        System.out.println("Perimeter of square = " + square.getPerimeter());
        System.out.println("");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------------");
        Rectangle rectangle2 = new Rectangle(2.2f, 4.4f);
        System.out.println(rectangle2); 
        //new rectangle value of l and w
        rectangle2.setLength(5.8f);
        rectangle2.setWidth(9.9f);
        System.out.println(rectangle2); 
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Rectangle Length is: " + rectangle2.getLength());
        System.out.println("Rectangle Width is: " + rectangle2.getWidth());
        System.out.printf("The Area of the Rectangle is: %.2f%n", rectangle2.getArea());
        System.out.printf("The Perimeter of the Rectangle is: %.2f%n", rectangle2.getPerimeter());
        System.out.println("------------------------------------------");
        
        
    }

}
