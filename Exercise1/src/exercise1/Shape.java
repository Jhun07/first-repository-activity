/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

/**
 *
 * @author 2ndyrGroupA
 */
public class Shape {

    private String color;
    private boolean filled;

    public Shape() {
        color = "green";
        filled = true;
    }

    public Shape(String colorSetIn, boolean filled) {
        this.color = color;
        this.filled = filled;

    }

    public String getColor() {
        return color;
    }

    public void setColor(String colorSetIn) {
        color = colorSetIn;
    }

    public boolean isFilled() {
        if (filled == true) {
            return true;
        } else {
            return false;
        }
    }

    public void setFilled(boolean filledSetIn) {
        filled = filledSetIn;
    }

    @Override
    public String toString() {
        String isNot = "";
        if (isFilled() == false) {
            isNot = "not ";
        }
        return "a Shape and has a color of  " + color + " and is " + isNot + " filled. ";
    }
}
