/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise4;

/**
 *
 * @author 2ndyrGroupA
 */
public class main {

     public static void main(String[] args) {
    
     Customer customer = new Customer(19105133, "Victoriano Moya Mulaan Jr.", 45);
     System.out.println(customer); 
     
     System.out.println("Customer id is: " + customer.getID());
     System.out.println("Customer name is: " + customer.getName());
     System.out.println("Customer discount is: " + customer.getDiscount());
     System.out.println("__________________________________________________________________________________________________________");
    
     
     Invoice invoice = new Invoice(69, customer, 500);
     System.out.println(invoice);
     

     System.out.println("__________________________________________________________________________________________________________");
     System.out.println("");
     System.out.println("Id is: " + invoice.getID());
     System.out.println("Customer's id is: " + invoice.getCustomerID());
     System.out.println("Customer's name is: " + invoice.getCustomerName());
     System.out.println("Amount is: " + invoice.getAmount());
     System.out.println("Customer's discount is: " + invoice.getCustomerDiscount());
     System.out.printf("Amount after discount is: %.2f%n", invoice.getAmountAfterDiscount());
  }
} 
