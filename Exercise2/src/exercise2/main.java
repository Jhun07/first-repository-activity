/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise2;

import java.util.Date;

/**
 *
 * @author 2ndyrGroupA
 */
public class main {

   public static void main(String[] args) {
      System.out.println("______________________________________________________");
      System.out.println("");
      Customer customer1 = new Customer("James Aldrin Brones");
      customer1.setMember(true);
      customer1.setMemberType("GOLD");
      System.out.println(customer1.toString());

      Visit visitor = new Visit("Armelita Moya",new Date());
      visitor.setProductExpense(2000);
      visitor.setServiceExpense(500);
      DiscountRate discounted = new DiscountRate();
      System.out.println("Service Discount is: "+visitor.getTotalExpense());
      

       System.out.println("______________________________________________________");
       System.out.println("");
       Visit visit1 = new Visit("Leordan Carmona",new Date());
       visit1.setProductExpense(322);
       visit1.setServiceExpense(450);
       System.out.println("Customer: "+visit1.getName());
       System.out.println("Product Expense: "+visit1.getProductExpense());
       System.out.println("Service Expense: "+visit1.getServiceExpense());
       System.out.println("Total Expense: "+visit1.getTotalExpense());
       System.out.println(visit1.toString());
       
       
       System.out.println("_____________________________________________________________________________________________");
       System.out.println("\nCustomer Testing:\n");
       Customer customer2 = new Customer("Quency Atacador");
       customer2.setMemberType("Premium");
       System.out.println("Customer: "+customer2.getName()); 
       System.out.println("Member Type: "+customer2.getMemberType());
       System.out.println("Is Member: "+customer2.isMember());
       System.out.println(customer2.toString());
       System.out.println("");
     
       
 
      
       System.out.println("___________________________________________");
       System.out.print("");
       System.out.println("\nTest for Discount Rates:\n");
       DiscountRate premium = new DiscountRate();
       System.out.println("The service discount rate of Premium is "+premium.getServiceDiscountRate("Premium"));
       System.out.println("The product discount rate of Premium is "+ premium.getProductDiscountRate("Premium"));
       DiscountRate gold = new DiscountRate();
       System.out.println("The service discount rate of Gold is "+ gold.getServiceDiscountRate("Gold"));
       System.out.println("The product discount rate of Gold is "+ premium.getProductDiscountRate("Gold"));
       DiscountRate silver = new DiscountRate();
       System.out.println("The service discount rate of Silver is "+ silver.getServiceDiscountRate("Silver"));
       System.out.println("The product discount rate of Silver is "+ premium.getProductDiscountRate("Silver"));
       System.out.print("____________________________________________");
}
}